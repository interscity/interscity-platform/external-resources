require 'mongo'
require 'singleton'

class MongoDB
  include Singleton
  attr_accessor :client, :db

  def initialize
    project_path = File.dirname(__FILE__)
    config = YAML.load_file("#{project_path}/settings.yml")
    @client = Mongo::Client.new([ config["DATABASE_HOST"] ], :database => config["DATABASE_NAME"])
    @db = @client.database
  end
end
