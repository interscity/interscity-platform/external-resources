#!/usr/bin/env ruby
#encoding: utf-8

require 'rubygems'
require 'date'
require 'json'
require 'rest-client'
require_relative 'mongo_db'
require_relative 'olho_vivo'

class Bus
  attr_accessor :bus_id, :line_id, :uuid, :accessible
  attr_accessor :lat, :lon, :description, :capabilities, :status

  def initialize(params={})
    params.each do |key, value|
      instance_variable_set("@#{key}", value)
    end
    self.status = "active"
    self.description = "Ônibus com identificador #{self.bus_id}"
    self.capabilities = ["bus_monitoring"]
  end

  def add_uuid(new_uuid)
    collection = MongoDB.instance.client[:buses]
    collection.update_one({bus_id: self.bus_id}, '$set' => {uuid: new_uuid})
  end

  def registered?
    !self.uuid.nil?
  end

  def data
    {
      lat: self.lat,
      lon: self.lon,
      description: self.description,
      capabilities: self.capabilities,
      status: self.status
    }
  end

  def self.collection(collection_name = :buses)
    MongoDB.instance.client[collection_name.to_sym]
  end

  def self.find_by_id(bus_id)
    bus = Bus.collection.find(bus_id: bus_id).first
    bus.nil? ? nil : Bus.new(bus)
  end
end

