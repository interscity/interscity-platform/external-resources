require 'date'
require_relative 'mongo_db'
require_relative 'olho_vivo'
require_relative 'bus'
require 'thread/pool'

class BusLine
  attr_accessor :route_id, :service_id, :trip_id, :headsign
  attr_accessor :direction_id, :shape_id, :shape, :frequencies
  attr_accessor :code, :circular, :direction
  attr_accessor :uuid
  attr_accessor :lat, :lon, :description, :capabilities, :status

  def initialize(args={})
    args.to_h.each do |k,v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
    self.lat = self.shape.first[:lat]
    self.lon = self.shape.first[:lon]
    self.status = "active"
    self.description = "Linha de ônibus #{trip_id} no sentido de #{headsign}"
    self.capabilities = ["bus_line_monitoring", "bus_line_metadata"]
  end

  def add_uuid(new_uuid)
    collection = MongoDB.instance.client[:bus_lines]
    collection.update_one({code: self.code}, '$set' => {uuid: new_uuid})
  end
  
  def registered?
    !self.uuid.nil?
  end

  def data
    {
      lat: self.lat,
      lon: self.lon,
      description: self.description,
      capabilities: self.capabilities,
      status: self.status
    }
  end

  def metadata
    {
      route_id: self.route_id,
      service_id: self.service_id,
      bus_line_id: self.trip_id,
      headsign: self.headsign,
      direction: self.direction_id,
      shape_id: self.shape_id,
      shape: self.shape,
      frequencies: self.frequencies,
      sptrans_code: self.code,
      circular: self.circular
    }
  end

  def create_trip_status(bus_positions, timestamp)
    document = {
      sptrans_code: self.code,
      bus_line: self.trip_id,
      direction: self.direction_id,
      headsign: self.headsign,
      number_of_buses: bus_positions.count,
      timestamp: timestamp,
      bus_positions: bus_positions,
      type: "bus_line",
    }
  end

  def self.collection(collection_name = :bus_lines)
    MongoDB.instance.client[collection_name.to_sym]
  end

  def self.find_by_code(code)
    bus_line = BusLine.collection.find(code: code).first
    bus_line.nil? ? nil : BusLine.new(bus_line)
  end

  def self.get_positions
    api = OlhoVivo.new
    positions = api.get_positions
    return nil if positions.nil?

    time = positions["hr"]
    hour = time[0..1]
    minute = time[3..4]
    today = Time.now
    timestamp = Time.new(today.year, today.month, today.day, hour, minute, 0, today.utc_offset)

    documents = []
    lines = positions["l"]
    lines.each do |line|
      code = line["cl"].to_i
      bus_line = BusLine.find_by_code(code)
      next if bus_line.nil?
      bus_positions = []
      line['vs'].each do |bus_info|
        bus_positions << {
          bus_id: bus_info['p'],
          accessible: bus_info['a'],
          timestamp: Time.parse(bus_info['ta']),
          location: {
            lat: bus_info['py'].to_f,
            lon: bus_info['px'].to_f,
          }
        }
        bus = Bus.find_by_id(bus_positions.last[:bus_id])
        if bus.nil?
          bus_doc = {bus_id: bus_positions.last[:bus_id], accessible: bus_positions.last[:accessible], lat: bus_positions.last[:location][:lat], lon: bus_positions.last[:location][:lon]}  
          result = Bus.collection.insert_one(bus_doc)
        end
      end
      documents << bus_line.create_trip_status(bus_positions, timestamp)
    end
    collection = MongoDB.instance.client[:bus_line_monitoring]
    collection.insert_many(documents)
    BusLine.post_positions(documents) if PlatformSetup.use_interscity?
  end

  def self.post_positions(documents)
    pool = Thread.pool(10)
    documents.each do |document|
      pool.process do 
        bus_line = BusLine.find_by_code(document[:sptrans_code])
        data = document
        PlatformSetup.post_data(bus_line, "bus_line_monitoring", data)

        #document[:bus_positions].each do |bus_position|
        #  bus = Bus.find_by_id(bus_position[:bus_id])
        #  created = PlatformSetup.create_resource(bus)
        #  next unless created
        #  bus = Bus.find_by_id(bus_position[:bus_id])
        #  data = bus_position
        #  data[:bus_line] = document[:bus_line]
        #  PlatformSetup.post_data(bus, "bus_monitoring", data)
        #end
      end
    end
    pool.shutdown
  end
end
