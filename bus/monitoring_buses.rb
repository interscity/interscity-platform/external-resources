#!/usr/bin/env ruby
#encoding: utf-8

require 'rubygems'
require 'date'
require 'json'
require 'rest-client'
require_relative 'mongo_db'
require_relative 'olho_vivo'
require_relative 'bus'
require_relative 'bus_line'
require_relative 'platform_setup'
require 'thread/pool'


capabilities = [
  {
    name: "bus_monitoring",
    description: "Provides data related to a bus, such as its current location",
    type: "sensor"
  },
  {
    name: "bus_line_monitoring",
    description: "Provides data related to a bus line, such as the current number of buses operating on the line and their location",
    type: "sensor"
  },
  {
    name: "bus_line_metadata",
    description: "Contains static data related to bus lines, such as their frequencies and shape ",
    type: "sensor"
  },
]

capabilities.each do |capability|
  PlatformSetup.setup_capability(capability[:name], capability[:description], capability[:type])
end

pool = Thread.pool(3)
while true
  pool.process do 
    BusLine.get_positions
  end
  sleep 120
end
pool.shutdown

