#!/usr/bin/env ruby
#encoding: utf-8

require 'rubygems'
require 'date'
require 'json'
require 'rest-client'
require 'colorize'

module PlatformSetup
  def self.use_interscity?
    ENV["USE_INTERSCITY"] == true || ENV["USE_INTERSCITY"] == "true" 
  end

  def self.interscity_url
    ENV["INTERSCITY"] ||= "localhost:8000"
  end

  def self.cataloguer_url
    ENV["CATALOGUER_HOST"] ||= "#{PlatformSetup.interscity_url}/catalog"
  end

  def self.adaptor_url
    ENV["ADAPTOR_HOST"] ||= "#{PlatformSetup.interscity_url}/adaptor"
  end

  def self.setup_capability(name, description, type = "sensor")
    if name.nil? || name.empty?
      raise "Capability name can't be blank"
    end

    begin
      RestClient.get(PlatformSetup.cataloguer_url + "/capabilities/#{name}")
      puts "Capability #{name} already exists".green
    rescue RestClient::ExceptionWithResponse => e
      if e.http_code == 404
        PlatformSetup.create_capability(name, description, type)
      else
        raise "Error while verifying for capability #{name}: #{e.response}"
      end
    end
  end

  def self.create_capability(name, description, type = "sensor")
    response = RestClient.post(
      PlatformSetup.cataloguer_url + "/capabilities",
      {
        name: name,
        description: description,
        capability_type: type
      }
    )
    puts "Capability #{name} created".green
    return response
  end

  def self.create_resource(resource)
    if resource.registered?
      return true
    end
    response = RestClient.post(
      PlatformSetup.adaptor_url + "/components",
      {data: resource.data}
    )
    resource.add_uuid(JSON.parse(response.body)['data']['uuid'])
    return true
  rescue RestClient::Exception => e
    puts "Could not register resource: #{e.response}".red
    return false
  end

  def self.post_data(resource, capability, data)
    if !resource.registered?
      created = PlatformSetup.create_resource(resource)
      return false unless created
    end

    data = [data] unless data.is_a? Array
    data_json = {}
    data_json[capability] = data
    response = RestClient.post(
      PlatformSetup.adaptor_url + "/components/#{resource.uuid}/data",
      {data: data_json}
    )
    return true
  rescue RestClient::Exception => e
    puts "Could not post data: #{e.response}".red
    return false
  end
end

