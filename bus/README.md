# Running bus script

## Setup

* Install Ruby 2.3.0
* Install the required gems: `bundle install`

## Start collector

To add GTFS data into MongoDB, run:
```sh
USE_INTERSCITY=true INTERSCITY=localhost:8000 ruby seed.rb
```

In order to run the script to collect data from 
[Olho Vivo API](http://www.sptrans.com.br/desenvolvedores/APIOlhoVivo.aspx)
you need to set a environment variable with the host of Resource Adaptor.
Thus, you may run the following:
```sh
USE_INTERSCITY=true INTERSCITY=localhost:8000 ruby monitoring_buses.rb
```

## TODO

* Serialize data to store the relation between bus id and resource uuid

## Testing API with CURL

* Authenticate:
```sh
curl -X POST http://api.olhovivo.sptrans.com.br/v2.1/Login/Autenticar?token=8fde5a257cd4a2df7ad59d38fe3b0b3f5f7094479b967d087110100a58545091 -d "" -c cookies.txt
```
* Find line:
```sh
curl http://api.olhovivo.sptrans.com.br/v2.1/Linha/Buscar?termosBusca=875C-10 -b cookies.txt | json_pp
```
