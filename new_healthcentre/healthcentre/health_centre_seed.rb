require 'csv'
require 'geocoder'
# require 'faker'
require_relative 'utils'
require 'json'
require 'rest-client'

ENV["INTERSCITY"] ||= "172.19.5.29:8000"
ENV["CATALOGUER_HOST"] ||= "#{ENV["INTERSCITY"]}/catalog"
ENV["ADAPTOR_HOST"] ||= "#{ENV["INTERSCITY"]}/adaptor"

def platform_setup
  name = "medical_procedure"
  existing_capabilities = JSON.parse(RestClient.get(ENV["CATALOGUER_HOST"] + "/capabilities"))
  existing_capabilities['capabilities'].each do |capability|
    if capability["name"] == name
      puts "Capability #{name} already exists"
      return true
    end
  end

  begin
    response = RestClient.post(
      ENV["CATALOGUER_HOST"] + "/capabilities",
      {
        name: name,
        description: "Provides data about medical procedures performed in a city resource, usually a health center.",
        capability_type: "sensor"
      }
    )
    puts "Capability #{name} created"
    return true
  rescue StandardError => e
    puts "Could not register resource: #{e.response}"
    return false
  end
end

def my_db
  db = DB.new

  health_centre_csv_path = File.join(__dir__, "csv/health_centres_real.csv")
  health_centre_types_csv_path = File.join(__dir__, "csv/health_centres_types.csv")
  specialties_csv_path = File.join(__dir__, "csv/specialties.csv")
  types_csv_path = File.join(__dir__, "csv/type.csv")
  counter = 0

  puts "LOAD HEALTH_CENTRES: "
  CSV.foreach(health_centre_csv_path, :headers => true) do |row|
    description = "Health Centre with CNES #{row[0]} NAME #{row[1]} BEDS #{row[2]} LONG #{row[3]} LAT #{row[4]}"
    hc = HealthCentre.new cnes: row[0], description: description, lat: row[4], lon: row[3]
    db.health_centres[hc.cnes] = hc
    counter += 1
    print "."
  end
  puts "Total Health Centre: ",  counter

  puts "\nLOAD SPECIALTIES: "
  CSV.foreach(specialties_csv_path, :headers => false) do |row|
    s = Specialty.new id: row[0], name: row[1]
    db.specialties[s.id] = s
    print "."
  end

  puts "\nLOAD HEALTH CENTRE TYPE: "
  CSV.foreach(types_csv_path, :headers => false) do |row|
    t = Type.new(id: row[0], name: row[1])
    db.types[t.id] = t
    print "."
  end

  puts "\nASSOCIATE TYPE WITH HEALTHCENTRE: "
  CSV.foreach(health_centre_types_csv_path, :headers => false) do |row|
    t = db.types[row[1]]
    hc = db.health_centres[row[0]]
    if hc != nil 
      hc.types.push(t)
    end
    print "."
  end

  puts "\n\n"
  db
end

########################
# Random Procedures code


def send_procedure db
  specialities_code = {
    1 => "CIRURGIA",
    2 => "OBSTETRECIA",
    3 => "CLINICA MEDICA",
    4 => "PACIENTES SOB CUIDADOS PROLONGADOS",
    5 => "PSIQUIATRIA",
    6 => "TISIOLOGIA",
    7 => "PEDIATRIA",
    8 => "REABILITACAO",
    9 => "PSIQUIATRIA EM HOSPITAL DIA",
    10 => "CLÍNICA CIRÚRGICA - HOSPITAL-DIA",
    11 => "AIDS - HOSPITAL-DIA",
    12 => "FIBROSE CÍSTICA - HOSPITAL-DIA",
    13 => "INTERCORRÊNCIA PÓS-TRANSPLANTE - HOSPITAL-DIA",
    14 => "GERIATRIA - HOSPITAL-DIA",
    15 => "SAÚDE MENTAL - HOSPITAL-DIA",
    16 => "SAÚDE MENTAL - CLÍNICO",
    17 => "NÃO DISCRIMINADO",
  }

  code = {
    1 => "Ambulatórios Especializados",
    2 => "Apoio Diagnóstico",
    3 => "Saúde Mental",
    4 => "Vigilância em Saúde",
    5 => "UBS/Posto de Saúde/Centro de Saúde",
    6 => "Urgência/ Emergência",
    7 => "Hospital",
    8 => "Unidades DST/AIDS",
    9 => "Outros Estabelecimentos e Serviços Especializados",
  }

  treatment_code = {
    1 => "ELETIVO",
    2 => 'URGÊNCIA',
    3 => 'ACIDENTE NO LOCAL TRABALHO OU A SERVIÇO DA EMPRESA',
    4 => 'ACIDENTE NO TRAJETO PARA O TRABALHO',
    5 => 'OUTROS TIPOS DE ACIDENTE DE TRÂNSITO',
    6 => 'OUTROS TIPOS LESÕES/ENVENENAMENTOS',
  }

  procedure_csv_path = File.join(__dir__, "csv/procedures.csv")
  puts "\nPROCEDURE"
  counter = 0;
  CSV.foreach(procedure_csv_path, :headers => true) do |row|
    hc = db.health_centres[row[9]]
    if hc != nil
      p = Procedure.new(cnes_id: row[9], lat: row[1], long: row[2], gender: row[3], age: row[4], date: row[13], specialty:  specialities_code[row[17].to_i], cid_primary: row[19], cid_secondary: row[20], cid_associated: row[21], treatment_type: treatment_code[row[11].to_i])
      hash_data = p.to_hash
      hc.send_data(hash_data)
      counter += 1
    end
  end
  puts "Total Procedures: ", counter
end

def seed
  platform_setup
  puts "SEEDING: "
  db = my_db
  db.health_centres.values.each do |hc|
    hc.register
  end
  send_procedure db
  puts "done"
  # db.save
end

seed
